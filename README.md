# Summary
The Ubuntu Base Server role sets up the foundation for the machines that are set up to use Ubuntu OS. This role is used to setup the following:

* Sets up the hostnames, default folders and basic network settings  
* Sets up the users based on the application and environments
* New Relic monitoring at the server level. This will monitor the CPU, disk, network and memory
* Network File Sharing for native Linux file sharing over the network
* Create bridged networking with 2 different network segments. Activated by the bridged-networking tag
* Creates the deploy admin user for standardized installs

# Additional settings
## Developer settings
Typically a variable with the list of developers is stored in inventories/group_vars/all/global.yml
The format of the hash looks like this
```
  development_users:
  - {
      username: "kelvin",
      ssh_public_key_file: "files/key_files/kelvin_kang.pub",
      password: "[hashed password]"
    }
```


# Variables
| Variable name            | Data Type | Description                                                                  | File   |
|--------------------------|-----------|------------------------------------------------------------------------------|--------|
| server_hostname_internal | string    | Internal hostname of the server                                              | host   |
| server_hostname_external | string    | External hostname of the server                                              | host   |
| external_dns_zone        | string    | DNS Zone for DNS Servers like Cloudflare                                     | host   |
| backup_directory         | string    | Default backup directory                                                     | global |
| temp_directory           | string    | Default temp directory                                                       | global |

# Settings
| Variable name            | Description                            | Values                                          | File   |
|--------------------------|----------------------------------------|-------------------------------------------------|--------|
| external_networking      | Sets up for external access            | true; false                                     | host   |

# Sample host_var file
```
---
  server_hostname_internal: web-sites-harbinger
  server_hostname_external: harbinger
  external_dns_zone: firsttiger.info

  network_adapters:
    - name: "green_network"
      interface: "ens18"
      ip_address: "10.1.3.7"
      subnet: "255.255.255.248"
      mask_bit: "29"
      gateway_address: "38.99.139.110"
      dhcp_option: "false"

    - name: "red_network"
      interface: "ens19"
      ip_address: "38.74.1.147"
      subnet: "255.255.255.240"
      mask_bit: "28"
      gateway_address: "38.74.1.158"
      dns_address: "10.1.0.1"
      dhcp_option: "false"

  #Options
  external_networking: true
  proxy_type: "traefik"
```